Implement  sorting using:  (a) heap  sort, (b)  binary tree  sort, (c)
quick sort,  and (d) merge  sort.  Measure the  time taken to  sort by
each technique and plot the time taken as a function of input size.

From the graph:

(a) What  do you observe as  the behaviour of heap  sort compared with
    binary tree sort?
(b) Repeat (a) for merge sort and quick sort.
(c) Explain your observations for heap sort and binary tree sort.  Did
    any data structuring technique contribute to your conclusions from
    the observations?
(d) Repeat (c) for merge and quick sorts.